from const import *

import random

class Agent():
    def __init__(self):
        pass

    def __str__(self):
        return "Agent name"

    def step(self, glitter, stench, breeze, bump, scream):
        return random.choice((WALK, ROTATE_LEFT, ROTATE_RIGHT, FIRE, CLIMB, LOOT))